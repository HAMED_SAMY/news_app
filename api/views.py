
from django.shortcuts import render
from rest_framework import generics
from articles.models import Article,Comment
from users.models import CustomUser
from .serializers import ArticleSerializer,CommentSerializer,UsersSerializer
from .permissions import IsAuthorOrReadOnly
# Create your views here.

class NewsAPIView(generics.ListCreateAPIView):
    queryset= Article.objects.all()
    serializer_class=ArticleSerializer
    

class NewsAPIView1(generics.RetrieveUpdateDestroyAPIView):
    queryset= Article.objects.all()
    serializer_class=ArticleSerializer
    permission_classes = (IsAuthorOrReadOnly)

class CommentsAPIView(generics.ListCreateAPIView):
    queryset=Comment.objects.all()
    serializer_class=CommentSerializer

class CommentsAPIView1(generics.RetrieveUpdateDestroyAPIView):
    queryset=Comment.objects.all()
    serializer_class=CommentSerializer
    permission_classes = (IsAuthorOrReadOnly)

class UsersAPIView(generics.ListCreateAPIView):
    queryset=CustomUser.objects.all()
    serializer_class=UsersSerializer

class UsersAPIView1(generics.RetrieveUpdateDestroyAPIView):
    queryset=CustomUser.objects.all()
    serializer_class=UsersSerializer
    permission_classes = (IsAuthorOrReadOnly)

