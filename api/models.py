
from django.db import models
from django.conf import settings
# Create your models here.
class Api(models.Model):
    title=models.CharField(max_length=100)
    body=models.TextField()
    created_by=models.ForeignKey(settings.AUTH_USER_MODEL,on_delete=models.CASCADE,default=1)

    def __str__(self):
        return self.title
    
